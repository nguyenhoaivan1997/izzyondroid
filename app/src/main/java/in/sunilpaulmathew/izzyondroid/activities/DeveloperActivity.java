package in.sunilpaulmathew.izzyondroid.activities;

import android.os.Bundle;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;

import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.izzyondroid.utils.Common;
import in.sunilpaulmathew.izzyondroid.utils.RecyclerViewData;
import in.sunilpaulmathew.izzyondroid.utils.tasks.DeveloperAppsLoadingTask;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on September 02, 2021
 */
public class DeveloperActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_developer);

        AppCompatImageButton mBack = findViewById(R.id.back);
        ProgressBar mProgress = findViewById(R.id.progress);
        MaterialTextView mTitle = findViewById(R.id.title);
        RecyclerView mRecyclerView = findViewById(R.id.recycler_view);

        mTitle.setText(getString(R.string.apps_by_developer, Common.getAuthorName()));

        mRecyclerView.setLayoutManager(new GridLayoutManager(this, RecyclerViewData.getSpanCount(6, 3, this)));
        new DeveloperAppsLoadingTask(mProgress, mRecyclerView, this).execute();

        mBack.setOnClickListener(v -> finish());
    }

}