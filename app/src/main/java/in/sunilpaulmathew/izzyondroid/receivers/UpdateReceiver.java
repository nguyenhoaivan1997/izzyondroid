package in.sunilpaulmathew.izzyondroid.receivers;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.izzyondroid.utils.AppSettings;
import in.sunilpaulmathew.izzyondroid.utils.PackageData;
import in.sunilpaulmathew.sCommon.CommonUtils.sExecutor;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on June 18, 2022
 */
public class UpdateReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        // Update repo information
        new sExecutor() {

            private NotificationManager mNotificationManager;

            @Override
            public void onPreExecute() {
                NotificationChannel mNotificationChannel = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    mNotificationChannel = new NotificationChannel("updateCheck", context.getString(R.string.app_name), NotificationManager.IMPORTANCE_DEFAULT);
                }
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, "updateCheck");
                Notification mNotification = mBuilder.setContentTitle(context.getString(R.string.app_name))
                        .setContentText(context.getString(R.string.refreshing))
                        .setPriority(Notification.PRIORITY_DEFAULT)
                        .setSmallIcon(R.drawable.ic_update)
                        .setOnlyAlertOnce(true)
                        .setAutoCancel(true)
                        .setSilent(true)
                        .build();

                mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    mNotificationManager.createNotificationChannel(mNotificationChannel);
                }
                try {
                    mNotificationManager.notify(0, mNotification);
                } catch (NullPointerException ignored) {}
            }

            @Override
            public void doInBackground() {
                PackageData.acquireRepoData(false, null, null, context);
            }

            @Override
            public void onPostExecute() {
                mNotificationManager.cancelAll();
                AppSettings.showUpdateNotification(context);
            }
        }.execute();
    }
    
}